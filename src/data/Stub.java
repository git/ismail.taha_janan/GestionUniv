/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package data;

import gestionuniv.BranchesListe;
import gestionuniv.Branche;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author istahajana
 */
public class Stub {
    
    public List<Branche> branchesData(){
        List<Branche> brancheList = new  ArrayList<>();
        brancheList.add(new Branche(){{
            nom="hello";  
        }});
        brancheList.add(new Branche(){{
            nom="hello";  
        }});
        brancheList.add(new Branche(){{
            nom="hello";  
        }});
        brancheList.add(new Branche(){{
            nom="hello";  
        }});
        brancheList.add(new Branche(){{
            nom="hello";  
        }});
        
        return brancheList;

    };
    
}
